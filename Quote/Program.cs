﻿using System;

namespace Quote
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var csvPath = args[0];
            var lendersRepository = new LenderCSVRepository(csvPath);
            var quoteCalculate = new QuoteCalculator(lendersRepository);
            var loanAmount = int.Parse(args[1]);
            var numberOfMonths = 36;
            var loanQuote = quoteCalculate.GetQuote(loanAmount,numberOfMonths);

            Console.WriteLine($"Requested amount: {loanAmount:C0}".Replace(",",""));
            Console.WriteLine($"Rate: {loanQuote.Rate:P1}");
            Console.WriteLine($"Monthly repayment: {loanQuote.MonthlyPayment:C}");
            Console.WriteLine($"Total repayment: {loanQuote.TotalAmountPayable:C}".Replace(",",""));
        }
    }
}