﻿namespace Quote
{
    public class LoanQuote
    {
        public double Rate { get; set; }
        public double MonthlyPayment { get; set; }
        public double TotalAmountPayable { get; set; }
    }
}