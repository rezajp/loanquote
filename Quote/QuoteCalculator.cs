﻿using System;
using System.ComponentModel;
using System.Linq;

namespace Quote
{
    public class QuoteCalculator
    {
        private readonly ILendersRepository _lendersRepository;

        public QuoteCalculator(ILendersRepository lendersRepository)
        {
            _lendersRepository = lendersRepository;
        }

        private double CalculateRate(int amount)
        {
            if (amount % 100 != 0)
            {
                throw new InvalidLoanAmountException(amount);
            }

            var lenders = _lendersRepository.GetLenders();
            if (amount > lenders.Sum(l => l.AvailableFund))
            {
                throw new NoAvailableFoundsException();
            }
            var sortedFunds =
                lenders.OrderBy(l => l.Rate).ThenBy(l => l.AvailableFund);

            var remainingAmount = amount;
            double collectiveInterest = 0;
            foreach (var lender in sortedFunds)
            {
                var availableFund = Math.Min(lender.AvailableFund, remainingAmount);
                collectiveInterest += availableFund * lender.Rate;
                remainingAmount -= availableFund;
                if (remainingAmount == 0)
                {
                    break;
                }
            }

            return Math.Round(collectiveInterest / amount, 2);
        }

        public LoanQuote GetQuote(int loanAmount, double numberOfMonths)
        {
            var rate = CalculateRate(loanAmount);
            var monthlyPayment = CalculateRepayment(loanAmount, numberOfMonths, rate);

            var totalAmountPayable = monthlyPayment * numberOfMonths;
            return new LoanQuote()
            {
                Rate = rate,
                MonthlyPayment = monthlyPayment,
                TotalAmountPayable = totalAmountPayable
            };
        }

        private double CalculateRepayment(int loanAmount, double numberOfMonths, double rate)
        {
            if (rate == 0)
            {
                return loanAmount / numberOfMonths;
            }
            var monthlyInterestRate = MonthlyInterestRate(rate);
            //formula from https://en.wikipedia.org/wiki/Mortgage_calculator
            var monthlyPayment = monthlyInterestRate * loanAmount /
                                 (1 - Math.Pow(1 + monthlyInterestRate, numberOfMonths * -1));
            return Math.Round(monthlyPayment, 2);
        }

        private double MonthlyInterestRate(double rate)
        {
            return rate / 12;
        }
    }
}