﻿using System;

namespace Quote
{
    public class InvalidLoanAmountException : Exception
    {
        private readonly int _amount;

        public InvalidLoanAmountException(int amount)
        {
            _amount = amount;
        }
        
    }
}