﻿namespace Quote
{
    public class Lender
    {
        public int AvailableFund { get; set; }
        public double Rate { get; set; }
        public string Name { get; set; }
    }
}