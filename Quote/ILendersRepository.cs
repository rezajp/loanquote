﻿namespace Quote
{
    public interface ILendersRepository
    {
        Lender[] GetLenders();
    }
}