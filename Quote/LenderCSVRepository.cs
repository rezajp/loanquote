﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Quote
{
    public class LenderCSVRepository : ILendersRepository
    {
        private readonly string _csvPath;
        private const string Separator = ",";

        public LenderCSVRepository(string csvPath)
        {
            _csvPath = csvPath;
        }
        public Lender[] GetLenders()
        {
            var lines = File.ReadAllLines(_csvPath);
            var lenders = new List<Lender>();
            for (var i = 1; i < lines.Length; i++)
            {
                try
                {
                    var cells = lines[i].Split(new[] {Separator}, StringSplitOptions.RemoveEmptyEntries);
                    lenders.Add(new Lender()
                    {
                        Name = cells[0],
                        Rate = double.Parse(cells[1]),
                        AvailableFund = int.Parse(cells[2])
                    });

                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

            return lenders.ToArray();
        }
    }
}