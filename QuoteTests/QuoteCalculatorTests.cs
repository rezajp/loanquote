﻿using System;
using Moq;
using NUnit.Framework;
using Quote;

namespace QuoteTests
{
    [TestFixture]
    public class QuoteCalculatorTests
    {
        [Test]    
        public void NoAvailableFundsIsThrownWhenSumOfLendersOffersAreLessThanRequestedLoan()
        {
            var lendersRepositoryMock = new Mock<ILendersRepository>();
            lendersRepositoryMock.Setup(l => l.GetLenders()).Returns(new[]
            {
                new Lender(){AvailableFund = 900},new Lender(){AvailableFund = 1099},  
            });
            var quoteCalculator = new QuoteCalculator(lendersRepositoryMock.Object);
              Assert.Throws<NoAvailableFoundsException>(   ()=>  quoteCalculator.GetQuote(2000,1));
        }
        [Test]    
        public void InvalidLoanAmountExceptionIsThrownWhenSumOfLendersOffersAreLessThanRequestedLoan()
        {
            var lendersRepositoryMock = new Mock<ILendersRepository>();
            var quoteCalculator = new QuoteCalculator(lendersRepositoryMock.Object);
            Assert.Throws<InvalidLoanAmountException>(   ()=>  quoteCalculator.GetQuote(910,1));
        }

        [Test]
        public void RateOf7PercentIsProposedWhenAmountAvailableAndBestRateIs7()
        {
            var lendersRepositoryMock = new Mock<ILendersRepository>();
            lendersRepositoryMock.Setup(l => l.GetLenders()).Returns(new[]
            {
                new Lender(){Rate=0.075,AvailableFund=640},
                new Lender(){Rate=0.069,AvailableFund=480},
                new Lender(){Rate=0.071,AvailableFund=520},
                new Lender(){Rate=0.104,AvailableFund=170},
                new Lender(){Rate=0.081,AvailableFund=320},
                new Lender(){Rate=0.074,AvailableFund=140},
                new Lender(){Rate=0.071,AvailableFund=60}
            });
            var quoteCalculator = new QuoteCalculator(lendersRepositoryMock.Object);
            var result =   quoteCalculator.GetQuote(1000,9);
            Assert.AreEqual(0.070d,result.Rate);
        }
        [Test]
        public void MonthlyPaymentIs3078GivenDataAndNumberOfInstallmentsIs36()
        {
            var lendersRepositoryMock = new Mock<ILendersRepository>();
            lendersRepositoryMock.Setup(l => l.GetLenders()).Returns(new[]
            {
                new Lender(){Rate=0.075,AvailableFund=640},
                new Lender(){Rate=0.069,AvailableFund=480},
                new Lender(){Rate=0.071,AvailableFund=520},
                new Lender(){Rate=0.104,AvailableFund=170},
                new Lender(){Rate=0.081,AvailableFund=320},
                new Lender(){Rate=0.074,AvailableFund=140},
                new Lender(){Rate=0.071,AvailableFund=60}
            });
            var quoteCalculator = new QuoteCalculator(lendersRepositoryMock.Object);
            var result =   quoteCalculator.GetQuote(1000,36);
            Assert.AreEqual(30.88,result.MonthlyPayment);
        }
        [Test]
        public void MonthlyPaymentIs100GivenDataAndNumberOfInstallmentsIs12()
        {
            var lendersRepositoryMock = new Mock<ILendersRepository>();
            lendersRepositoryMock.Setup(l => l.GetLenders()).Returns(new[]
            {
                new Lender(){Rate=0.0,AvailableFund=640},
                new Lender(){Rate=0.069,AvailableFund=480},
                new Lender(){Rate=0.0,AvailableFund=520},
                new Lender(){Rate=0.104,AvailableFund=170},
                new Lender(){Rate=0.0,AvailableFund=320},
                new Lender(){Rate=0.074,AvailableFund=140},
                new Lender(){Rate=0.071,AvailableFund=60}
            });
            var quoteCalculator = new QuoteCalculator(lendersRepositoryMock.Object);
            var result =   quoteCalculator.GetQuote(1200,12);
            Assert.AreEqual(100,result.MonthlyPayment);
        }
    }
}